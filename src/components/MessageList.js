import React from "react";
import {Timeline} from "antd";

const MessageList = ({messages}) => (
    <Timeline>{messages.map((m, i) =>
        <Timeline.Item key={i}>{m}</Timeline.Item>
    )}</Timeline>
);

export default MessageList;
