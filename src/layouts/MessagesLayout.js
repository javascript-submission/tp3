import React from "react";
import {Layout} from "antd";
import MessageList from "../components/MessageList";
import InputMessage from "../components/InputMessage";

class MessageLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: []
        }
        this.addMessage = this.addMessage.bind(this)
    }

    addMessage(message) {
        this.state.messages.push(message)
    }

    render() {
        return (
            <Layout style={{height: "calc(100%)"}}>
                <Layout.Content style={{padding: "0 50px", height: "calc(80%)"}}>
                    <Layout
                        style={{
                            padding: "24px 0",
                            background: "#fff",
                            height: "calc(100%)"
                        }}
                    >
                        <Layout.Content style={{padding: "0 24px", height: "calc(80%)"}}>
                            <MessageList messages={["Test", "Test2"]}/>
                        </Layout.Content>
                    </Layout>
                </Layout.Content>
                <Layout.Footer
                    style={{
                        padding: "0 50px",
                        textAlign: "center",
                        height: "calc(20%)"
                    }}
                >
                    <InputMessage onSubmit={() => this.addMessage}/>
                </Layout.Footer>
            </Layout>
        );
    }
}

export default MessageLayout;
